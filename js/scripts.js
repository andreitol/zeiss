// Initialize Tabbis == tabs toggle
var tabs = tabbis.init();


// 0. Initialize Swiper Vertical screen + Mousewheel Control
var swiper1 = new Swiper('.swiper-container1', {
	direction: 'vertical',
	slidesPerView: 1,
	spaceBetween: 0,
	mousewheel: true,
	pagination: {
		el: '.swiper-pagination1',
		clickable: true,
		// type: 'custom',
		// bulletClass: '.vertical-pagination'
	},
});


/* Initialize Swiper
4. History */
var swiper41 = new Swiper('.slider-history-img', {
	slidesPerView: 1,
	spaceBetween: 30,
	// nested: true,
	pagination: {
		el: '.pagination-history',
		clickable: true,
	},
});

// 4. History - History texts
var swiper42 = new Swiper('.slider-history-texts', {
	slidesPerView: 1,
	spaceBetween: 30,
	pagination: {
		el: '.pagination-history',
		clickable: true,
	},
	navigation: {
		nextEl: '.history-texts-button-next',
		prevEl: '.history-texts-button-prev'
	},
});

/* Initialize Swiper Custom Pagination + Navigation
6. Building plan */
var swiper6 = new Swiper('.plan-slider', {
	navigation: {
		nextEl: '.plan-slider-button-next',
		prevEl: '.plan-slider-button-prev',
	},
	effect: 'fade',
	fadeEffect: {
		crossFade: true
	},
	pagination: {
		el: '.plan-slider-pagination',
		clickable: true,
		renderBullet: function (index, className) {
			return '<span class="' + className + ' plan-pag-active ">' + (index + 1) + '</span>';
		},
	},
});